// system
#include <iostream>
#include <string>
// smeeze image
#include <smeeze/image/factory.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
void test_read(const std::string &filename) {
  const auto img(smeeze::image::factory::read(filename));
  std::cout << "image: " << filename << std::endl;
  std::cout << " width... " << img.get_width() << std::endl;
  std::cout << " height.. " << img.get_height() << std::endl;
}
} // namespace
///////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv) {
  for (int n = 1; n < argc; ++n) {
    test_read(argv[n]);
  }
  return 0;
}
