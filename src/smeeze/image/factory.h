#pragma once

// system
#include <string>
// smeeze image
#include <smeeze/image/image.h>

namespace smeeze {
namespace image {
class factory {
public:
  static smeeze::image::image read(const std::string &);
  static void write(const std::string &, const smeeze::image::image &);
};
} // namespace image
} // namespace smeeze
