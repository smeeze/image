#pragma once

// system
#include <cassert>
// system
#include <vector>

namespace smeeze {
namespace image {
class image {
public:
  struct pixel_type {
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;
  };

  using data_type = std::vector<pixel_type>;

  image(int width, int height, const data_type &data)
      : _width(width), _height(height), _data(data) {}
  image(int width, int height)
      : _width(width), _height(height), _data(_width * _height) {}

  const auto &get_width() const { return _width; }
  const auto &get_height() const { return _height; }
  const auto &get_data() const { return _data; }

  const pixel_type &operator()(const int &x, const int &y) const {
    assert(x >= 0);
    assert(x < _width);
    assert(y >= 0);
    assert(y < _height);
    return operator[](y *_width + x);
  }
  pixel_type &operator()(const int &x, const int &y) {
    assert(x >= 0);
    assert(x < _width);
    assert(y >= 0);
    assert(y < _height);
    return operator[](y *_width + x);
  }
  const pixel_type &operator[](int n) const { return _data[n]; }
  pixel_type &operator[](int n) { return _data[n]; }

  auto begin() const { return _data.cbegin(); }
  auto end() const { return _data.cend(); }
  auto begin() { return _data.begin(); }
  auto end() { return _data.end(); }

  void argb_to_rgba() {
    for (auto &p : _data) {
      std::swap(p.r, p.g);
      std::swap(p.g, p.b);
      std::swap(p.b, p.a);
    }
  }

private:
  int _width;
  int _height;
  data_type _data;
};
} // namespace image
} // namespace smeeze
