#pragma once

// system
#include <vector>
// gpx++
#include <smeeze/image/image.h>

namespace smeeze {
namespace image {
namespace png {
class image {
public:
  static smeeze::image::image read(const std::vector<uint8_t> &);
  static std::vector<uint8_t> write(const smeeze::image::image &);
};
} // namespace png
} // namespace image
} // namespace smeeze
