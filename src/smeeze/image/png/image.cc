// system
#include <cstring>
#include <stdexcept>
// libpng
extern "C" {
#include <png.h>
}
// smeeze views
#include <smeeze/views/helpers/generic_view.h>
#include <smeeze/views/zip.h>
// gpx++
#include <smeeze/image/png/image.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
//
//
//
typedef struct {
  const png_uint_32 width;
  const png_uint_32 height;
  const int color_type;
} _libpng_info;
//
//
//
typedef struct {
  const png_byte *data;
  const png_size_t size;
} _libpng_data_handle;
//
//
//
typedef struct {
  const _libpng_data_handle data;
  png_size_t offset;
} _libpng_read_data_handle;
//
//
//
void read_png_data_callback(png_structp png_ptr, png_byte *raw_data,
                            png_size_t read_length) {
  auto handle(static_cast<_libpng_read_data_handle *>(png_get_io_ptr(png_ptr)));
  const png_byte *png_src = handle->data.data + handle->offset;
  memcpy(raw_data, png_src, read_length);
  handle->offset += read_length;
}
//
//
//
void write_png_data_callback(png_structp png_ptr, png_bytep data,
                             png_size_t length) {
  std::vector<uint8_t> *p = (std::vector<uint8_t> *)png_get_io_ptr(png_ptr);
  p->insert(p->end(), data, data + length);
}
//
//
//
_libpng_info read_and_update_info(const png_structp png_ptr,
                                  const png_infop info_ptr) {
  png_uint_32 width, height;
  int bit_depth, color_type;

  png_read_info(png_ptr, info_ptr);
  png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type,
               NULL, NULL, NULL);

  // Convert transparency to full alpha
  if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
    png_set_tRNS_to_alpha(png_ptr);

  // Convert grayscale, if needed.
  if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
    png_set_expand_gray_1_2_4_to_8(png_ptr);

  // Convert paletted images, if needed.
  if (color_type == PNG_COLOR_TYPE_PALETTE)
    png_set_palette_to_rgb(png_ptr);

  // Add alpha channel, if there is none (rationale: GL_RGBA is faster than
  // GL_RGB on many GPUs)
  if (color_type == PNG_COLOR_TYPE_PALETTE || color_type == PNG_COLOR_TYPE_RGB)
    png_set_add_alpha(png_ptr, 0xFF, PNG_FILLER_AFTER);

  // Ensure 8-bit packing
  if (bit_depth < 8)
    png_set_packing(png_ptr);
  else if (bit_depth == 16)
    png_set_scale_16(png_ptr);

  png_read_update_info(png_ptr, info_ptr);

  // Read the new color type after updates have been made.
  color_type = png_get_color_type(png_ptr, info_ptr);

  return _libpng_info{width, height, color_type};
}
} // namespace
///////////////////////////////////////////////////////////////////////////////
smeeze::image::image
smeeze::image::png::image::read(const std::vector<uint8_t> &data) {
  auto png_data_size(data.size());
  auto png_data((png_const_bytep)data.data());

  if (!png_check_sig(png_data, 8)) {
    throw(std::runtime_error("not a png buffer"));
  }
  png_structp png_ptr =
      png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  assert(png_ptr != NULL);
  png_infop info_ptr = png_create_info_struct(png_ptr);
  assert(info_ptr != NULL);

  _libpng_read_data_handle png_data_handle =
      _libpng_read_data_handle{{png_data, png_data_size}, 0};

  png_set_read_fn(png_ptr, &png_data_handle, read_png_data_callback);

  if (setjmp(png_jmpbuf(png_ptr))) {
    throw(std::runtime_error("error reading png"));
  }

  _libpng_info png_info = read_and_update_info(png_ptr, info_ptr);

  smeeze::image::image img(png_info.width, png_info.height);
  std::vector<uint8_t *> rows(img.get_height());
  for (int y = 0; y < img.get_height(); ++y) {
    rows[y] = (uint8_t *)&img(0, y);
  }

  png_read_image(png_ptr, rows.data());

  png_destroy_info_struct(png_ptr, &info_ptr);
  png_destroy_read_struct(&png_ptr, &info_ptr, NULL);

  return img;
}
///////////////////////////////////////////////////////////////////////////////
std::vector<uint8_t>
smeeze::image::png::image::write(const smeeze::image::image &img) {
  auto p(png_create_write_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr,
                                 nullptr));
  auto info_ptr(png_create_info_struct(p));
  png_set_IHDR(p, info_ptr, img.get_width(), img.get_height(), 8,
               PNG_COLOR_TYPE_RGBA, PNG_INTERLACE_NONE,
               PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

  std::vector<uint8_t *> rows(img.get_height());
  for (int y = 0; y < img.get_height(); ++y) {
    rows[y] = (uint8_t *)&img(0, y);
  }
  png_set_rows(p, info_ptr, &rows[0]);

  std::vector<uint8_t> rv;
  png_set_write_fn(p, &rv, write_png_data_callback, nullptr);
  png_write_png(p, info_ptr, PNG_TRANSFORM_IDENTITY, nullptr);

  png_destroy_info_struct(p, &info_ptr);
  png_destroy_write_struct(&p, nullptr);

  return rv;
}
