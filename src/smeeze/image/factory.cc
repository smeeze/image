// system
#include <fstream>
// smeeze image
#include <smeeze/image/factory.h>
#include <smeeze/image/png/image.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
std::vector<uint8_t> read_file(const std::string &filename) {
  std::vector<uint8_t> buf;
  std::ifstream in(filename, std::ios::in);
  if (in) {
    in.seekg(0, std::ios::end);
    buf.resize(in.tellg());
    in.seekg(0, std::ios::beg);
    in.read((char *)buf.data(), buf.size());
    in.close();
  } else {
    throw(std::runtime_error("could not open file"));
  }

  return buf;
}
///////////////////////////////////////////////////////////////////////////////
void write_file(const std::string &filename,
                               const std::vector<uint8_t> &buffer) {
  std::ofstream out(filename, std::ios::out);
  out.write(reinterpret_cast<const char *>(buffer.data()), buffer.size());
}
}
///////////////////////////////////////////////////////////////////////////////
smeeze::image::image smeeze::image::factory::read(const std::string &filename) {
  return smeeze::image::png::image::read(read_file(filename));
}
///////////////////////////////////////////////////////////////////////////////
void smeeze::image::factory::write(const std::string &filename,
                                   const smeeze::image::image &img) {
  write_file(filename, smeeze::image::png::image::write(img));
}
