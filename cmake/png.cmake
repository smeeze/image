include(FetchContent)
FetchContent_Declare(
  png
  URL https://download.sourceforge.net/libpng/libpng-1.6.16.tar.gz
)

FetchContent_GetProperties(png)
if(NOT png_POPULATED)
  FetchContent_Populate(png)
  set(png_SOURCE_DIR ${png_SOURCE_DIR})
endif()

add_library(png STATIC
  ${png_SOURCE_DIR}/png.c
  ${png_SOURCE_DIR}/pngerror.c
  ${png_SOURCE_DIR}/pngget.c
  ${png_SOURCE_DIR}/pngmem.c
  ${png_SOURCE_DIR}/pngpread.c
  ${png_SOURCE_DIR}/pngread.c
  ${png_SOURCE_DIR}/pngrio.c
  ${png_SOURCE_DIR}/pngrtran.c
  ${png_SOURCE_DIR}/pngrutil.c
  ${png_SOURCE_DIR}/pngset.c
  ${png_SOURCE_DIR}/pngtrans.c
  ${png_SOURCE_DIR}/pngwio.c
  ${png_SOURCE_DIR}/pngwtran.c
  ${png_SOURCE_DIR}/pngwutil.c
  ${png_SOURCE_DIR}/pngwrite.c
)
target_compile_definitions(png PRIVATE PNG_WEIGHT_SHIFT=8)
target_compile_definitions(png PRIVATE PNG_COST_SHIFT=3)
target_include_directories(png SYSTEM PUBLIC ${png_SOURCE_DIR})
target_link_libraries(png zlib)
