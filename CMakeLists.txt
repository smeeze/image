cmake_minimum_required(VERSION 3.11)

project(smeeze-image)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

include(cmake/ccache.cmake)
include(cmake/clang.cmake)
include(cmake/gcc.cmake)
include(cmake/msvc.cmake)

include(cmake/zlib.cmake)
include(cmake/png.cmake)
include(cmake/smeeze-views.cmake)

add_library(smeeze-image STATIC
  src/smeeze/image/factory.cc
  src/smeeze/image/png/image.cc
)
target_include_directories(smeeze-image PUBLIC src)
target_include_directories(smeeze-image PRIVATE ${SMEEZE_VIEWS_INCLUDE_DIR})
target_link_libraries(smeeze-image PRIVATE png)

add_executable(test-read test/read.cc)
target_link_libraries(test-read PRIVATE smeeze-image)
